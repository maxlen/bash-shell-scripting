####### Lesson 17 (redirect) #######
echo "put some text for to file" > textFile.txt # redirect/write text to file
echo "this text for new file1" > file1.txt # again to other file
echo "add more text to file wich already exist" >> textFile.txt # for add text to end of file
ls -l . >> file1.txt # redirect output(list of files) to file1.txt
cat textFile.txt > file2.txt # redirect output of cat-command to file2.txt
cat textFile.txt file1.txt > file2.txt # concatinate files and redirect output to file2.txt

####### Lesson 18 (pipe "|") #######
ls A-folder/ | less # show list of items from folder A-folder and output list by less-command
ls A-folder/ > somefile.txt # put list of items from folder A-folder to file somefilr.txt
ls A-folder/ | tail -3 # show last 3 lines from output
ls A-folder/ | tail -3 | less # show last 3 lines from output by less-command
ls A-folder/ | tail -3 | sort  > somefile.txt # show last 3 lines from output, sort them and put to file somefile.txt

####### Lesson 19 (find) #######
find ~/Downloads/ -name Dockerfile # find item(file or folder) Dockerfile in folder ~/Downloads
find ~/Downloads/Dockerfile # find same as above
find ~/Downloads/ # find all items in folder "Downloads"
find ~/Downloads/ -name 1.t* # find all items in folder "~/Downloads/" by pattern "1.t*"
find ~/Downloads/ -type file # find only files in folder "~/Downloads/"


