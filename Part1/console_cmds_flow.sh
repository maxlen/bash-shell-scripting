####### Lesson 1/2 (terminal overview) #######
date # date
sudo apt install gnustep-gui-runtime # install say
say "hello man" # and run say hello :)
date # a command can act alone
echo "hello people" # a command can act on something
date -u # a command can have options
cal 02 2020 # show calendar
cal # or

####### Lesson 4 (ls) #######
whoami # show name of current user
pwd # where am i
ls # list of catalogs|files
ls -a # list all files (also hidden)

####### Lesson 5 (Understand the Terminal Output) #######
ls -l # list all files (with more details)
# - - - Rights descriptions - - -:
# drwxrwxr-x 4 maxlrnash maxlrnash 4096 лют 11 09:16 Part1
# -rw-r--r-- 1 maxlrnash maxlrnash 2622 лют 11 09:00 README.md
# Explanations:
# d - is a directory
# - - is a file
# rwx    r-x    r-x    (read, write, execute)
# owner  group  other

####### Lesson 6 (Ls with folders) #######
ls -la Part1/ # list folder
file README.md # tell me more about file
# README.md: UTF-8 Unicode text, with very long lines
# and
# tell me more about image
file bash.jpg
# bash.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, Exif Standard: [TIFF image data, big-endian, direntries=8, orientation=upper-left, xresolution=110, yresolution=118, resolutionunit=2, software=GIMP 2.8.22, datetime=2020:02:11 09:36:53], progressive, precision 8, 800x343, frames 3

####### Lesson 7/8 (locating and open somethig) #######
pwd # show me where am i
cd myFolder # replace me to directory myFolder
cd # move me to my home directory
cd .. # go to parent directory
cd ../.. # go to up twice
open someDirectory # opened directory or file in new window (gui)
xdg-open ml-test-file.txt # opened|execute file (gui)

####### Lesson 9 (touch) #######
touch test.txt # create file test.txt

####### Lesson 10/11 (folders create, delete, rename, move and copy) #######
mkdir newDir # create directory newDir
touch first.txt second.txt third.html # created 3 files
cd .. # go to parent dir
mkdir dirA dirB # created 2 directories
mv newDir/first.txt dirA/first.txt # move file from newDir to dirA
mv dirB dirC # rename dirB to dirC
cp newDir/second.txt dirC/cp-second.txt
rm newDir/third.html

####### Lesson 12 (sign star: *) #######
touch b1.txt b.html d4.html c2.php # lets create some files
file *.html # show me files with '.html' at the end of name
rm b* # remove all file starts with 'b'

####### Lesson 13 (recursive) #######
cp -R newDir/ dirC/ # copy all data from newDir to dirC
rm -R dirC # remove all data from dirC
mv dirA/ dirB/ # don't need to flag "-R"
ls -R someDir/ # will show contains of someDir recursively



